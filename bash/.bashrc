# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm|xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
# force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

# Git prompt configuration
if [ -e "/usr/share/git/completion/git-prompt.sh" ]; then
    source "/usr/share/git/completion/git-prompt.sh"
fi

GIT_PS1_DESCRIBE_STYLE='describe'
GIT_PS1_SHOWCOLORHINTS='y' # Doesn't seem to work
GIT_PS1_SHOWDIRTYSTATE='y'
GIT_PS1_SHOWSTASHSTATE=
GIT_PS1_SHOWUNTRACKEDFILES='y'
GIT_PS1_SHOWUPSTREAM='auto'

git_ps1=""

if [ -n "$(LC_ALL=C type -t __git_ps1)" ] && [ "$(LC_ALL=C type -t __git_ps1)" = function ]; then
    # __git_ps1 available
    git_ps1="\$(__git_ps1 ' (%s)')"
fi

function nonzero_return() {
    RETVAL=$?
    [ $RETVAL -ne 0 ] && echo " [$RETVAL] "
}

function hashCode() {
    local foo="$1"
    local -i h=0
    for ((i = 0; i < ${#foo}; i++)); do

        printf -v val "%d" "'${foo:$i:1}"  # val is ASCII val

        if ((31 * h + val > 2147483647))   # hash scheme
        then
            h=$((-2147483648 + (31 * h + val) % 2147483648 ))
        elif ((31 * h + val < -2147483648))
        then
            h=$(( 2147483648 - ( 31 * h + val) % 2147483648 )) 
        else
            h=$(( 31 * h + val))
        fi
    done

    printf "%d" $h
}


HOSTNAME=`hostname`
HOST_HASH=`hashCode $HOSTNAME`
HOST_COLOR="$(( $HOST_HASH % 7))"
HOST_COLOR="$(( $HOST_COLOR + 91 ))"


pcol_usr='\[\e[36m\]'
# Set root user to red in color p�rompt
if [ "$EUID" -eq 0 ]; then
    pcol_usr='\[\e[91m\]'
fi

if [ "$color_prompt" = yes ]; then
    PS1="${debian_chroot:+($debian_chroot)}\[\e[33m\][\[\e[m\]${pcol_usr}\u\[\e[m\]\[\e[33m\]@\[\e[m\]\[\e[${HOST_COLOR}m\]\h\[\e[m\]\[\e[33m\]]\[\e[m\]:\[\e[94m\]\w\[\e[m\]\[\e[36m\]${git_ps1}\[\e[31m\]\`nonzero_return\`\[\e[m\]${pcol_usr}\\$\[\e[m\] "
#    PS1="${debian_chroot:+($debian_chroot)}\[${pcol_brackets}[ ${pcol_usr}\u${pcol_brackets}@${pcol_host}\h ${pcol_brackets}]${pcol_r}\] ${pcol_dir}\w${pcol_usr}\$${pcol_r} "
else
    PS1="${debian_chroot:+($debian_chroot)}[\u@\h]:\W\\$ "
fi
unset pcol_usr color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -lsah'
alias la='ls -A'
alias l='ls -lsh'

# Elevate but keep env
alias elev='HOME=/root sudo -E -s'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Less / man colors
export LESS_TERMCAP_mb=$(tput bold; tput setaf 3)
export LESS_TERMCAP_md=$(tput bold; tput setaf 6)
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_so=$(tput bold; tput setaf 3; tput setab 4)
export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
export LESS_TERMCAP_us=$(tput bold; tput setaf 2)
export LESS_TERMCAP_ue=$(tput sgr0)
export LESS_TERMCAP_mr=$(tput rev)
export LESS_TERMCAP_mh=$(tput dim)
export LESS_TERMCAP_ZN=$(tput ssubm)
export LESS_TERMCAP_ZV=$(tput rsubm)
export LESS_TERMCAP_ZO=$(tput ssupm)
export LESS_TERMCAP_ZW=$(tput rsupm)
export GROFF_NO_SGR=1         # For Konsole and Gnome-terminal


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
