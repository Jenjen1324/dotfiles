#!/bin/bash

export DOTFILES_TARGET="$HOME"
export DOTFILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

dotfiles_setup () {
    local target=$DOTFILES_TARGET
    local dot=$DOTFILES_DIR

    echo "Setting up dotfiles in $target from $dot"

    # Set up symlinks
    echo " > bash config"
    ln -s -f "$dot/bash/.bashrc" "$target/.bashrc"

    echo " > vim config (removing existing .vim/)"
    ln -s -f "$dot/vim/.vimrc" "$target/.vimrc"

    if [[ -d "$target/.vim" ]]; then
        rm -r "$target/.vim"
    fi
    ln -s -f "$dot/vim/.vim" "$target/.vim"

    echo " > tmux config"
    ln -s -f "$dot/.tmux.conf" "$target/.tmux.conf"
}

dotfiles_setup
./ssh/setup_ssh.sh

unset dotfiles_setup

