#!/bin/bash



echo "Setting up SSH in ${DOTFILES_TARGET}"

if [[ ! -d "${DOTFILES_TARGET}/.ssh" ]]; then
    echo "> Creating .ssh directory"
    mkdir "${DOTFILES_TARGET}/.ssh"
fi

echo "> Setting directory permissions"
chmod 700 "${DOTFILES_TARGET}/.ssh"

if [[ ! -f "${DOTFILES_TARGET}/.ssh/authorized_keys" ]]; then
    echo "> Creating authorized keys file"
    touch "${DOTFILES_TARGET}/.ssh/authorized_keys"
fi

echo "> Setting file permissions"
chmod 644 "${DOTFILES_TARGET}/.ssh/authorized_keys"

echo "> Removing public key"

target_file="${DOTFILES_TARGET}/.ssh/authorized_keys"


cat $target_file | tr '\n' '\f' | sed -e 's/#### BEGIN\: auto dotfiles.*#### END\: auto dotfiles//g' | tr '\f' '\n' > $target_file
#echo "#### BEGIN: auto dotfiles" >> $target_file
#cat "${DOTFILES_DIR}/ssh/authorized_keys" >> $target_file
#echo "#### END: auto dotfiles" >> $target_file

