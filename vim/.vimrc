
"  let g:netrw_banner = 0
"  let g:netrw_liststyle = 3
"  let g:netrw_browse_split = 4
"  let g:netrw_altv = 1
"  let g:netrw_winsize = 25
"  
"  augroup ProjectDrawer
"    autocmd!
"    autocmd VimEnter * :Vexplore
"  augroup END

" Nerdtree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

execute pathogen#infect()
syntax on
filetype plugin indent on


" Remap P to paste from xsel if no gui/clipboard is available
command! XselPaste execute 'r !xsel -po'
command! -range=% -nargs=0 XselCopy :silent exec "<line1>,<line2>w !xsel -b"

if !has('xterm_clipboard') && !has('gui_running')
    nnoremap <silent> P :XselPaste<CR>
    vnoremap <silent> Y :XselCopy<CR>
endif

command! RVim execute 'so $MYVIMRC'



" Navigation in Panes
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

map <C-n> :NERDTreeToggle<CR>

set tabstop=4
set shiftwidth=4
set number
set relativenumber
set laststatus=2
set cursorline
set mouse=a
set scrolloff=5
set t_Co=256
set background=dark
colorscheme PaperColor

set expandtab

set statusline=%<%f\ %y%h%m%r%=%-14.(%l,%c%V%)\ %P


autocmd Filetype yaml set tabstop=2
autocmd Filetype yaml set shiftwidth=2

